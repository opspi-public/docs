# OpsPi Docs

## Deployment

https://se-docs.apps.opspi.com/


## Getting started

- Install Sphinx:

```bash
pip install sphinx
```

- Create a directory inside your project to hold your docs:

```bash
cd /path/to/project
mkdir docs
```

- Run sphinx-quickstart in there:

```bash
cd docs
sphinx-quickstart
```

- Using Markdown with Sphinx

```bash
pip install myst-parser
```

- Update in your conf.py:

```bash
extensions = ['myst_parser']
```

- Build them to see how they look:

```bash
make html
```

## Additional

- Activate Poetry shell

```bash
poetry shell
```

