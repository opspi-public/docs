# Plans

> {sub-ref}`today` | {sub-ref}`wordcount-minutes` min read

## Super Admin

- Plans available to create
  - Reseller plan
    - This will create reseller account on ISPConfig
    - This will create reseller account on SolidCP

## Reseller

- Plans available to create
  - Linux Plan
    - This will create reseller account on ISPConfig
  - Windows Plan
    - This will create reseller account on ISPConfig
    - This will create reseller account on SolidCP

  - The domain selling should be enabled by default for each reseller.

## MORE INFO

- No option needed to create Domain Plan.
- No option needed to create HSphere Plan. Hsphere plan is for legacy users migration only. Plans should not be displayed to be purchased.
