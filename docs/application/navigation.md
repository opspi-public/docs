# Application Navigation

> {sub-ref}`today` | {sub-ref}`wordcount-minutes` min read

## Controls

### Form Buttons

You might encounter few buttons when working on a form or submitting information. The action for buttons should be interprated as below:

  - **Update**
    
    Save the current changes and stay on the edit page.

  - **Done**

    Save the changes and redirect to product listing page.

  - **Cancel**

    Discard the changes and redirect to product listing page.
