# Billing

> {sub-ref}`today` | {sub-ref}`wordcount-minutes` min read


## Billing Dates

- The next invoice should be generated on same day next month.
- If the date is not available on the next month, generate invoice on next available date.


## Scenario 1

- User purchase a plan on 10 April
- Plan Validity: 10 April - 09 May
- The next invoice would be generated 10 May

## Scenario 2

- User purchase a plan on 31 March
- Plan Validity: 31 March - 30 April
- The next invoice would be generated 1 May

## Scenario 3

- User purchase a plan on 31 January
- Plan Validity: 31 January - 28 Feb
- The next invoice would be generated 1 March

## Scenario 4

- User purchase a plan on 31 December

1. Bill cycle 01
  - Plan Validity: 31 December - 30 Jan
  - The next invoice would be generated 31 January
1. Bill cycle 02
  - Plan Validity: 31 January - 28 Feb (Not in Leap Year)
  - The next invoice would be generated 1 March
1. Bill cycle 03
  - Plan Validity: 1 March - 30 March
  - The next invoice would be generated 31 March
1. Bill cycle 04
  - Plan Validity: 31 March - 30 April
  - The next invoice would be generated 1 May


- We should need to note down the billing date, in Scenario 4 it would be 31st. We'll always try to bill on 31st or a day later if 31st is not available.
