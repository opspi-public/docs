# Application Environments

> {sub-ref}`today` | {sub-ref}`wordcount-minutes` min read


## Development

  - **Used Case**

    - When writing and testing code on local machine

  - **Who would use it?**

    - Developers

  - Pointer Domain

  ```bash
  pointer.dev.opspi.com
  ```

  - Custom domain for testing

  ```bash
  *.custom.dev.opspi.com
  ```


## Testing

  - **Used Case**

    - Running the test cases

  - **Who would use it?**

    - Developers
    - Automated/Manual testing tools

## QA

  - **Used Case**

    - Quality Assurance
    - Testing builds against new code
    - Testing deploys against new code
    - Testing/Validation for features delivered by developer.

  - **Who would use it?**

    - Developers
    - Testers
    - Team members incuded in standup calls and updates
    - Project Managers

  - Pointer Domain

  ```bash
  pointer.qa.opspi.com
  ```

  - Custom domain for testing

  ```bash
  *.custom.qa.opspi.com
  ```

## Staging

  - **Used Case**

    - Test against equivalent environment to production.
    - Test against the product similar data.
    - Final tests before release

  - **Who would use it?**

    - Project Managers
    - Testers
    - JodoHost Team

  - Pointer Domain

  ```bash
  pointer.stag.opspi.com
  ```

  - Custom domain for testing

  ```bash
  *.custom.stag.opspi.com
  ```


## Production

  - **Used Case**

    - Final release

  - **Who would use it?**

    - End Users

