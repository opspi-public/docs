# Redirect Workflow

> {sub-ref}`today` | {sub-ref}`wordcount-minutes` min read

## Redirection after login

### Case 1 (User reached the login page from main site by clicking login button)

- User should be redirected to user dashboard

## Case 2 (User reached the login page after mail confirmation)

- User should be redirected to user dashboard

## Case 3 (User reached the login page during checkout)

- User should be redirected to cart at checkout process (where the user left off)


## Redirection on visiting the homepage

### Case 1 (User is already logged in)


### Case 1 (User clicked the home link from control panel or someplace at website)


