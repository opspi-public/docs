# OpsPi - Leading the NextGen!

> {sub-ref}`today` | {sub-ref}`wordcount-minutes` min read

```{admonition} This is work in progress
:class: warning

Consider this document as draft
```

Indices and Tables
==================

* {ref}`Index <genindex>`
* {ref}`Module Index <modindex>`
* {ref}`Search Page <search>`


(section-two)=
## Reference

* [StackExpress](https://stackexpress.com)
* [APYL](https://www.apyl.com)


```{toctree}
:caption: Application
:hidden:
application/index
```

```{toctree}
:caption: Reseller
:hidden:
reseller/index
```

```{toctree}
:hidden:
:caption: End User
end_user/index.md
```


```{toctree}
:caption: Developers
:hidden:
developers/index.md
```
